#include "le.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

struct llist * create_l(){
    struct llist *head;
    head=malloc(sizeof(struct llist));
    if (head==NULL) {
        return NULL;
    }
    else
    {
        head->cabeca=NULL;
        head->tam=0;
        return head;
    }

    return NULL;

};

 elem * create_node(int val){
    struct elem *aux; 
     aux=malloc(sizeof(struct elem));
     if (aux==NULL){
         return NULL;
     }
     else{
        aux->val=val;
        aux->next=NULL;
        return aux;
     }
     return NULL;
 }

 int insert_l(struct llist *desc, elem * prev, elem * item){
     struct elem *aux;
    
    if (prev==NULL) {
        item->next=desc->cabeca;
        desc->cabeca=item;
        desc->tam++;
        return 1;
    }
    
    else
    {
        aux=prev->next;
        prev->next=item;
        item->next=aux;
        desc->tam++;
        return 1;
    }
    return 0;
   
 };


int delete_l(struct llist *desc, elem * prev){
    if (prev==NULL) {
        
        if (desc->cabeca==NULL) {
            return 0;
        }
        
        if (desc->cabeca->next==NULL) {
            free(desc->cabeca);
            desc->tam=0;
            return 1;
        }

        
        if (desc->cabeca->next!=NULL) {
            struct elem *aux;
            aux=prev->next;
            prev->next=aux->next;
            free(aux);
            desc->tam--;
            return 1;
        }   
    }
    else{
        struct elem *aux;
        aux=prev->next;
        prev->next=aux->next;
        free(aux);
        desc->tam--;
        return 1;
    }
    return 0;
};


elem * get_l(struct llist *desc, int pos){
    
    if (desc->cabeca==NULL) {
        return NULL;
    }
    
    else
    {
        struct elem *aux;
        int i=1;
        aux=desc->cabeca;
        while(aux!=NULL){
            if(i==pos){
                return aux;
            }
            i++;
            aux=aux->next;
        }  
    }
    return NULL;
    
};


int set_l(struct llist *desc, int pos, int val){
    if (desc->cabeca==NULL) {
        return 0;
    }
    
    else
    {
        struct elem *aux;
        int i=1;
        aux=desc->cabeca;
        while(aux!=NULL){
            if(i==pos){
                aux->val=val;
                return 1;
            }
            i++;
            aux=aux->next;
        }  
    }
    return 0;
};


elem * locate_l(struct llist *desc, elem * prev, int val){
    
    if (prev==NULL) {
        prev=desc->cabeca;
    }   
        
    while(prev!=NULL){
        if(prev->val==val){
            return prev;
        }
    prev=prev->next;
    }
    return NULL;
};


int length_l(struct llist *desc){
    return desc->tam;
};


void destroy_l(struct llist *desc){
    free(desc);
};

/* Aqui devem ser implementadas as funções definidas em le.h */