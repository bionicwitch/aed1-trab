#include "pilha.h"
#include <stdlib.h>
#define MAX 5
// typedef elem int;

/** Cria uma pilha
 * @return  um descritor ou NULL
 */
struct pilha * create(){
    struct pilha *novo;
    novo=malloc(sizeof(struct pilha));
    
    if (novo==NULL) {
        return NULL;
    }
    struct list *q;
    q=createls(MAX);//mudar nomes 
    novo->desc=q;
    return novo;
    
}
/** Apaga todos elementos da pilha
 * @param p descritor da pilha
 * @return 1 se OK, 0 se erro
 */
int makenull(struct pilha * p){
     if (p->desc==NULL) {
        return 0;
    }
    
    else
    {   
        for(int i=0;i<p->desc->ultimo;i++){
            p->desc->arm[i]=0;
            p->desc->ultimo--;
        }
        return 1;
    }
    return 0;
};

/* Retorna o elemento no topo da pilha, ou zero se não existir
 * @param p descritor da pilha
 * @return o elemento ou 0
 */
int top(struct pilha * p){
    if(p->desc->ultimo==0){
        return 0;
    }
    else{
        int aux=p->desc->arm[p->desc->ultimo-1];
       return aux;
    }
}

/* Descarta o topo da pilha
 * @param p descritor de pilha
 * @return 1 se OK, 0 se erro
 */
int pop(struct pilha * p){
     if(p->desc->ultimo==0){
        return 0;
    }
    else{
       removel(p->desc,p->desc->ultimo);
        return 1;
    }
    return 0;
};

/* Insere um elemento no topo da pilha
 * @param p descritor de pilha
 * @param val elemento a ser inserido
 * @return 1 se OK, 0 se erro
 */
int push(struct pilha * p, int val){
    
    if (p->desc==NULL||p->desc->ultimo==MAX) {
        return 0;
    }
    
    else
    {   
        insert(p->desc,p->desc->ultimo+1, val);
        return 1;
    }
    return 0;
};

/* Retorna se a pilha está vazia ou não
 * @param p descritor de pilha
 * @return 1 se vazia, 0 se não
 */
 int vazia(struct pilha *p){
    if (p->desc->ultimo==0) {
        return 1;
    }
    
    return 0;
 };
/** Desaloca toda a pilha
  * @param p descritor da pilha
  */
void destroy(struct pilha * p){
    destroyls(p->desc);

};
